import requests

class WeatherAPIHandler:
    def __init__(self, key):
        self.base_url = "http://api.weatherapi.com/v1"
        self.request_methods = {
            "current_weather": "/current.json",
            "forecast": "/forecast.json",
            "search": "/search.json",
            "history": "/history.json",
            "future": "/future.json",
            "time_zone": "/timezone.json",
            "sports": "/sports.json",
            "astronomy": "/astronomy.json"
        }
        self.key = key

    def get_current_weather(self, query):
        payload = {
            'key': self.key,
            'q': query
        }
        response = requests.get(self.get_request_url("current_weather"), params = payload)
        return(response.status_code, response.json())

    def get_forecast(self, query):
        payload = {
            'key': self.key,
            'q': query
        }
        response = requests.get(self.get_request_url("forecast"), params = payload)
        return(response.status_code, response.json())
        
    def get_search(self, query):
        payload = {
            'key': self.key,
            'q': query
        }
        response = requests.get(self.get_request_url("search"), params = payload)
        return(response.status_code, response.json())
        
    def get_history(self, query, dt):
        payload = {
            'key': self.key,
            'q': query
        }
        response = requests.get(self.get_request_url("history"), params = payload)
        return(response.status_code, response.json())
        
    def get_future(self, query, dt):
        payload = {
            'key': self.key,
            'q': query
        }
        response = requests.get(self.get_request_url("future"), params = payload)
        return(response.status_code, response.json())
        
    def get_time_zone(self, query):
        payload = {
            'key': self.key,
            'q': query
        }
        response = requests.get(self.get_request_url("time_zone"), params = payload)
        return(response.status_code, response.json())
        
    def get_sports(self, query):
        payload = {
            'key': self.key,
            'q': query
        }
        response = requests.get(self.get_request_url("sports"), params = payload)
        return(response.status_code, response.json())

    def get_astronomy(self, query):
        payload = {
            'key': self.key,
            'q': query
        }
        response = requests.get(self.get_request_url("astronomy"), params = payload)
        return(response.status_code, response.json())

    def get_request_url(self, method):
        return self.base_url + self.request_methods[method]