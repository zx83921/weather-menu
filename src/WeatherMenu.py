from WeatherAPIHandler import WeatherAPIHandler
from WeatherData import *
from os.path import exists

class WeatherMenu:
    def __init__(self,auto_load_key = True):
        if auto_load_key:
            key = self.load_key()
            if key == "Invalid":
                print("Failed to load key from key.txt.")
                key = self.input_key()
        else:
            key = self.input_key()
        self.handler = WeatherAPIHandler(key)
        self.key = key
        self.scale = "both"
        self.options = [
            ["Manually input API key", self.update_key_input],
            ["Load API key from key.txt", self.update_key_load],
            ["Save API key to key.txt", self.save_key],
            ["Toggle Imperial/Metric scale", self.toggle_scale],
            ["Get current weather data", self.print_current_weather],
            ["Get data for 14 day forecast", self.print_forecast],
            ["Search", self.quit],
            ["History", self.quit],
            ["Future", self.quit],
            ["Time Zone", self.quit],
            ["Sports", self.quit],
            ["Quit", self.quit]
            ]
        self.valid_keys, self.menu_txt = self.get_enum_menu()

    def get_enum_menu(self):
        keys = []
        menu = [i[0] for i in self.options]
        menu_txt = ""
        for i, v in enumerate(menu):
            keys.append(str(i+1))
            menu_txt += str(i+1) + ". " + v + "\n"
        return keys, menu_txt

    def update_key(self, key):
        if key != "cancel":
            print("Setting key to {0}".format(key))
            self.key = key
            self.handler.key = key
        return

    def update_key_input(self):
        self.print_bold_line()
        self.update_key(self.input_key())
        self.print_bold_line()
        return
    
    def update_key_load(self):
        self.print_bold_line()
        self.update_key(self.load_key())
        self.print_bold_line()
        return

    def input_key(self):
        return input("Input API Key (\"cancel\" to exit): ")

    def load_key(self):
        print("Loading key from key.txt")
        key = ""
        if exists("../key.txt"):
            with open("../key.txt", 'r') as f:
                key = f.read()
                print("Successfully loaded key file.")
        else:
            print("Failed to load key from key.txt.")
        return key

    def save_key(self):
        choice = self.ask_save_key()
        if choice == 'y':
            with open("../key.txt", 'w') as f:
                f.write(self.key)
                print("Saving key to key.txt.")
        else:
            print("Canceling save operation.")
        return


    def ask_save_key(self):
        choice = ''
        options = ['y', 'n']
        if exists('../key.txt'):
            with open('../key.txt', 'r') as f:
                key_in_file = f.read()
                action = "Overwrite"
        else:
            key_in_file = "None"
            action = "Save to"
        menu = "Current key: {0}\nKey in key.txt: {1}\n{2} key.txt? (y/n) ".format(self.key, key_in_file, action)
        while choice not in options:
            choice = input(menu).lower()
    
    def toggle_scale(self):
        self.print_bold_line()
        if self.scale == "both":
            print("Now using Imperial scale.")
            self.scale = "i"
        elif self.scale == "i":
            print("Now using Metric scale.")
            self.scale = "m"
        else:
            print("Now using both Imperial and Metric scale.")
            self.scale = "both"
        self.print_bold_line()
        return
    
    def print_current_weather(self):
        self.print_bold_line()
        query = input("Input query: ")
        self.print_line()
        status, data = self.handler.get_current_weather(query)
        if status == 200:
            weather_data = CurrentWeatherData(data)
            readable_data = weather_data.get_readable_all(self.scale)
            print("Data found for your query.")
            self.print_line()
            print(readable_data)
        else:
            print("Failed to retrieve data for your query.")
        self.print_bold_line()
        return

    def print_forecast(self):
        self.print_bold_line()
        query = input("Input query: ")
        self.print_line()
        status, data = self.handler.get_forecast(query)
        if status == 200:
            forecast_data = ForecastData(data)
            readable_data = forecast_data.get_readable_all(self.scale)
            print("Data found for your query.")
            self.print_line()
            print(readable_data)
        else:
            print("Failed to retrieve data for your query.")
        self.print_bold_line()
        return
    
    def print_line(self):
        print("-"*50)
        return
    
    def print_bold_line(self):
        print("="*50)
        return
    
    def quit(self):
        print("Exiting program...")

    def ask(self):
        choice = ''
        while choice not in self.valid_keys:
            print("Main Menu")
            self.print_line()
            menu = self.menu_txt + ("=" * 50) + "\n> "
            choice = input(menu)
        if choice == self.valid_keys[-1]:
            return -1
        return int(choice)

    def start(self):
        choice = 0
        self.print_bold_line()
        while choice != -1:
            choice = self.ask()
            self.options[choice - 1][1]()
        return