from WeatherMenu import WeatherMenu

def main():
    menu = WeatherMenu()
    menu.start()
    return

if __name__ == "__main__":
    main()