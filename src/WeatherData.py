from math import floor

class LocationData:
    def __init__(self, data):
        self.data = data
        self.location = self.data["location"]

    def get_readable_all(self):
        data = [
            self.get_readable_location()
        ]
        readable_data = "\n".join(data)
        return readable_data
    
    def get_readable_location(self):
        name = self.location["name"]
        region = self.location["region"]
        country = self.location["country"]
        return "Location: {0}, {1} {2}".format(name, region, country)

class CurrentWeatherData(LocationData):
    def __init__(self, data):
        super().__init__(data)
        self.last_updated = self.data["current"]["last_updated"]
        self.condition = self.data["current"]["condition"]
        self.is_day = self.data["current"]["is_day"]
        self.cloud_coverage = self.data["current"]["cloud"]
        self.uv_index = self.data["current"]["uv"]
        self.humidity_percentage = self.data["current"]["humidity"]
        self.temperature = {
            'i': self.data["current"]["temp_f"],
            'm': self.data["current"]["temp_c"]
        }
        self.feels_like = {
            'i': self.data["current"]["feelslike_f"],
            'm': self.data["current"]["feelslike_c"]
        }
        self.wind_speed = {
            'i': self.data["current"]["wind_mph"],
            'm': self.data["current"]["wind_kph"]
        }
        self.wind_dir = {
            "compass": self.data["current"]["wind_dir"],
            "degree": self.data["current"]["wind_degree"]
        }
        self.gust_speed = {
            'i': self.data["current"]["gust_mph"],
            'm': self.data["current"]["gust_kph"]
        }
        self.pressure = {
            'i': self.data["current"]["pressure_in"],
            'm': self.data["current"]["pressure_mb"]
        }
        self.precipitation = {
            'i': self.data["current"]["precip_in"],
            'm': self.data["current"]["precip_mm"]
        }
        
    def get_readable_all(self, scale = "both"):
        data = [
            self.get_readable_location(),
            self.get_readable_last_updated(),
            self.get_readable_is_day(),
            self.get_readable_condition(),
            self.get_readable_cloud_coverage(),
            self.get_readable_temperature(scale),
            self.get_readable_feels_like_temperature(scale),
            self.get_readable_humidity(),
            self.get_readable_uv_index(),
            self.get_readable_wind_speed(scale),
            self.get_readable_wind_dir(),
            self.get_readable_gust_speed(scale),
            self.get_readable_pressure(scale),
            self.get_readable_precipitation(scale)
        ]
        readable_data = "\n".join(data)
        return readable_data

    def get_readable_last_updated(self):
        return "Last updated on: {0}".format(self.last_updated)

    def get_readable_temperature(self, scale = "both"):
        temp_i = self.temperature["i"]
        temp_m = self.temperature["m"]
        if scale == "both":
            return "Temperature (F): {0}\nTemperature (C): {1}".format(temp_i, temp_m)
        elif scale == "i":
            return "Temperature (F): {0}".format(temp_i)
        elif scale == "m":
            return "Temperature (C): {0}".format(temp_m)
        else:
            raise Exception("Not a valid temperature scale.")
        
    def get_readable_feels_like_temperature(self, scale = "both"):
        temp_i = self.feels_like["i"]
        temp_m = self.feels_like["m"]
        if scale == "both":
            return "Feels Like Temperature (F): {0}\nFeels Like Temperature (C): {1}".format(temp_i, temp_m)
        elif scale == "i":
            return "Feels Like Temperature (F): {0}".format(temp_i)
        elif scale == "m":
            return "Feels Like Temperature (C): {0}".format(temp_m)
        else:
            raise Exception("Not a valid scale.")
        
    def get_readable_condition(self):
        return "Condition: {0}".format(self.condition["text"])
    
    def get_readable_is_day(self):
        return "It is currently {0} time".format("day" if self.is_day else "night")
    
    def get_readable_cloud_coverage(self):
        return "Cloud Coverage: {0}%".format(self.cloud_coverage)
    
    def get_readable_humidity(self):
        return "Humidity: {0}%".format(self.humidity_percentage)
    
    def get_readable_uv_index(self):
        return "UV Index: {0}".format(self.uv_index)
    
    def get_readable_wind_speed(self, scale = "both"):
        speed_i = self.wind_speed["i"]
        speed_m = self.wind_speed["m"]
        if scale == "both":
            return "Wind Speed (mph): {0}\nWind Speed (kph): {1}".format(speed_i, speed_m)
        elif scale == "i":
            return "Wind Speed (mph): {0}".format(speed_i)
        elif scale == "m":
            return "Wind Speed (kph): {0}".format(speed_m)
        else:
            raise Exception("Not a valid scale.")
        
    def get_readable_wind_dir(self):
        return "Wind Direction (compass): {0}\nWind Direction (degree): {1}".format(self.wind_dir["compass"], self.wind_dir["degree"])
    
    def get_readable_gust_speed(self, scale = "both"):
        speed_i = self.gust_speed["i"]
        speed_m = self.gust_speed["m"]
        if scale == "both":
            return "Gust Speed (mph): {0}\nGust Speed (kph): {1}".format(speed_i, speed_m)
        elif scale == "i":
            return "Gust Speed (mph): {0}".format(speed_i)
        elif scale == "m":
            return "Gust Speed (kph): {0}".format(speed_m)
        else:
            raise Exception("Not a valid scale.")
        
    def get_readable_pressure(self, scale = "both"):
        pressure_i = self.pressure["i"]
        pressure_m = self.pressure["m"]
        if scale == "both":
            return "Pressure (in): {0}\nPressure (mb): {1}".format(pressure_i, pressure_m)
        elif scale == "i":
            return "Pressure (in): {0}".format(pressure_i)
        elif scale == "m":
            return "Pressure (mb): {0}".format(pressure_m)
        else:
            raise Exception("Not a valid scale.")
        
    def get_readable_precipitation(self, scale = "both"):
        precipitation_i = self.precipitation["i"]
        precipitation_m = self.precipitation["m"]
        if scale == "both":
            return "Precipitation (in): {0}\nPrecipitation (mb): {1}".format(precipitation_i, precipitation_m)
        elif scale == "i":
            return "Precipitation (in): {0}".format(precipitation_i)
        elif scale == "m":
            return "Precipitation (mb): {0}".format(precipitation_m)
        else:
            raise Exception("Not a valid scale.")

class ForecastData(LocationData):
    def __init__(self, data):
        super().__init__(data)
        self.date = self.data["forecast"]["forecastday"][0]["date"]
        self.day = ForecastDayData(self.data["forecast"]["forecastday"][0]["day"])
        self.hour = ForecastHourData(self.data["forecast"]["forecastday"][0]["hour"])
        self.astro = ForecastAstroData(self.data["forecast"]["forecastday"][0]["astro"])

    def get_readable_all(self, scale = "both"):
        data = [
            self.date,
            self.get_readable_location(),
            "-"*50,
            "Daily Forecast",
            "-"*50,
            self.get_readable_day_data(scale),
            "-"*50,
            "Hourly Forecast",
            "-"*50,
            self.get_readable_hour_data(scale),
            "-"*50,
            "Astro Forecast",
            "-"*50,
            self.get_readable_astro_data(scale)
        ]
        readable_data = "\n".join(data)
        return readable_data
    
    def get_readable_day_data(self, scale = "both"):
        return self.day.get_readable_all(scale)
    
    def get_readable_hour_data(self, scale = "both"):
        return self.hour.get_readable_all(scale)
    
    def get_readable_astro_data(self, scale = "both"):
        return self.astro.get_readable_all(scale)
    
class ForecastDayData:
    def __init__(self, data):
        self.data = data
        self.temperature = {
            "max": {
                "i": self.data["maxtemp_f"],
                "m": self.data["maxtemp_c"]
            },
            "min": {
                "i": self.data["mintemp_f"],
                "m": self.data["mintemp_c"]
            },
            "avg": {
                "i": self.data["avgtemp_f"],
                "m": self.data["avgtemp_c"]
            }
        }
        self.max_wind_speed = {
            "i": self.data["maxwind_mph"],
            "m": self.data["maxwind_kph"]
        }
        self.total_precipitation = {
            "i": self.data["totalprecip_in"],
            "m": self.data["totalprecip_mm"] * 10
        }
        self.total_snow = {
            "i": self.cm_to_in(self.data["totalsnow_cm"]),
            "m": self.data["totalsnow_cm"]
        }
        self.avg_visibility = {
            "i": self.data["avgvis_miles"],
            "m": self.data["avgvis_km"]
        }
        self.avg_humidity = self.data["avghumidity"]
        self.will_rain = self.data["daily_will_it_rain"]
        self.chance_of_rain = self.data["daily_chance_of_rain"]
        self.will_snow = self.data["daily_will_it_snow"]
        self.chance_of_snow = self.data["daily_chance_of_snow"]
        self.condition = self.data["condition"]
        self.uv_index = self.data["uv"]

    def cm_to_in(self, cm):
        return int(floor(cm * 0.39))

    def get_readable_all(self, scale = "both"):
        data = [
            self.get_readable_max_temperature(scale),
            self.get_readable_min_temperature(scale),
            self.get_readable_avg_temperature(scale),
            self.get_readable_max_wind_speed(scale),
            self.get_readable_total_precipitation(scale),
            self.get_readable_total_snow(scale),
            self.get_readable_avg_visibility(scale),
            self.get_readable_avg_humidity(),
            self.get_readable_will_rain(),
            self.get_readable_chance_of_rain(),
            self.get_readable_will_snow(),
            self.get_readable_chance_of_snow(),
            self.get_readable_condition(),
            self.get_readable_uv_index()
            
        ]
        readable_data = "\n".join(data)
        return readable_data

    def get_readable_max_temperature(self, scale):
        temp_i = self.temperature["max"]['i']
        temp_m = self.temperature["max"]["m"]
        if scale == "both":
            return "Max Temperature (F): {0}\nMax Temperature (C): {1}".format(temp_i, temp_m)
        elif scale == "i":
            return "Max Temperature (F): {0}".format(temp_i)
        elif scale == "m":
            return "Max Temperature (C): {0}".format(temp_m)
        else:
            raise Exception("Not a valid scale.")
        
    def get_readable_min_temperature(self, scale):
        temp_i = self.temperature["min"]['i']
        temp_m = self.temperature["min"]["m"]
        if scale == "both":
            return "Min Temperature (F): {0}\nMin Temperature (C): {1}".format(temp_i, temp_m)
        elif scale == "i":
            return "Min Temperature (F): {0}".format(temp_i)
        elif scale == "m":
            return "Min Temperature (C): {0}".format(temp_m)
        else:
            raise Exception("Not a valid scale.")
        
    def get_readable_avg_temperature(self, scale):
        temp_i = self.temperature["avg"]['i']
        temp_m = self.temperature["avg"]["m"]
        if scale == "both":
            return "Average Temperature (F): {0}\nAverage Temperature (C): {1}".format(temp_i, temp_m)
        elif scale == "i":
            return "Average Temperature (F): {0}".format(temp_i)
        elif scale == "m":
            return "Average Temperature (C): {0}".format(temp_m)
        else:
            raise Exception("Not a valid scale.")
        
    def get_readable_max_wind_speed(self, scale):
        wind_speed_i = self.max_wind_speed['i']
        wind_speed_m = self.max_wind_speed["m"]
        if scale == "both":
            return "Max Wind Speed (mph): {0}\nMax Wind Speed (kph): {1}".format(wind_speed_i, wind_speed_m)
        elif scale == "i":
            return "Max Wind Speed (mph): {0}".format(wind_speed_i)
        elif scale == "m":
            return "Max Wind Speed (kph): {0}".format(wind_speed_m)
        else:
            raise Exception("Not a valid scale.")
        
    def get_readable_total_precipitation(self, scale):
        precip_i = self.total_precipitation['i']
        precip_m = self.total_precipitation["m"]
        if scale == "both":
            return "Total Precipitation (in): {0}\nTotal Precipitation (cm): {1}".format(precip_i, precip_m)
        elif scale == "i":
            return "Total Precipitation (in): {0}".format(precip_i)
        elif scale == "m":
            return "Total Precipitation (cm): {0}".format(precip_m)
        else:
            raise Exception("Not a valid scale.")
        
    def get_readable_total_snow(self, scale):
        snow_i = self.total_snow['i']
        snow_m = self.total_snow["m"]
        if scale == "both":
            return "Total Snow (in): {0}\nTotal Snow (cm): {1}".format(snow_i, snow_m)
        elif scale == "i":
            return "Total Snow (in): {0}".format(snow_i)
        elif scale == "m":
            return "Total Snow (cm): {0}".format(snow_m)
        else:
            raise Exception("Not a valid scale.")
        
    def get_readable_avg_visibility(self, scale):
        visibility_i = self.avg_visibility['i']
        visibility_m = self.avg_visibility["m"]
        if scale == "both":
            return "Average Visibility (mi): {0}\nAverage Visibility (km): {1}".format(visibility_i, visibility_m)
        elif scale == "i":
            return "Average Visibility (mi): {0}".format(visibility_i)
        elif scale == "m":
            return "Average Visibility (km): {0}".format(visibility_m)
        else:
            raise Exception("Not a valid scale.")
        
    def get_readable_avg_humidity(self):
        return "Average Humidity: {0}".format(self.avg_humidity)
    
    def get_readable_will_rain(self):
        return "There will{0} be rain in the next 14 days.".format("" if self.will_rain else " not")
    
    def get_readable_chance_of_rain(self):
        return "Chance of Rain: {0}%".format(self.chance_of_rain)
    
    def get_readable_will_snow(self):
        return "There will{0} be snow in the next 14 days.".format("" if self.will_snow else " not")

    def get_readable_chance_of_snow(self):
        return "Chance of Snow: {0}%".format(self.chance_of_snow)
    
    def get_readable_condition(self):
        return "Condition: {0}".format(self.condition["text"])
    
    def get_readable_uv_index(self):
        return "UV Index: {0}".format(self.uv_index)

class ForecastHourData:
    def __init__(self, data):
        self.data = data

    def get_readable_all(self, scale = "both"):
        return ""

class ForecastAstroData:
    def __init__(self, data):
        self.data = data
        self.sun_rise = self.data["sunrise"]
        self.sun_set = self.data["sunset"]
        self.sun_is_up = self.data["is_sun_up"]
        self.moon_rise = self.data["moonrise"]
        self.moon_set = self.data["moonset"]
        self.moon_phase = self.data["moon_phase"]
        self.moon_illumination = self.data["moon_illumination"]
        self.moon_is_up = self.data["is_moon_up"]

    def get_readable_all(self, scale = "both"):
        data = [
            self.get_readable_sun_rise(),
            self.get_readable_sun_set(),
            self.get_readable_sun_is_up(),
            self.get_readable_moon_rise(),
            self.get_readable_moon_set(),
            self.get_readable_moon_is_up(),
            self.get_readable_moon_phase(),
            self.get_readable_moon_illumination()
        ]
        readable_data = "\n".join(data)
        return readable_data
    
    def get_readable_sun_rise(self):
        return "The sun rises at {0}.".format(self.sun_rise)
    
    def get_readable_sun_set(self):
        return "The sun sets at {0}.".format(self.sun_set)

    def get_readable_sun_is_up(self):
        return "The sun is{0} up.".format("" if self.sun_is_up else " not")
    
    def get_readable_moon_rise(self):
        return "The moon rises at {0}.".format(self.moon_rise)
    
    def get_readable_moon_set(self):
        return "The moon sets at {0}.".format(self.moon_set)
    
    def get_readable_moon_is_up(self):
        return "The moon is{0} up.".format("" if self.moon_is_up else " not")
    
    def get_readable_moon_phase(self):
        return "The current moon phase is {0}.".format(self.moon_phase)
    
    def get_readable_moon_illumination(self):
        return "Moon Illumination: {0}%".format(self.moon_illumination)